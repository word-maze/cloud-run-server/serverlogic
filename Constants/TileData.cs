﻿using System.Text.Json.Serialization;

namespace ServerLogic.Constants
{
    public class Shape
    {
        [JsonPropertyName("vertices")]
        public long[][] Vertices { get; set; }

        [JsonPropertyName("scale")]
        public Scale Scale { get; set; }

        [JsonPropertyName("faces")]
        public Face[] Faces { get; set; }
    }

    public class Face
    {
        [JsonPropertyName("offset")]
        public double[] Offset { get; set; }

        [JsonPropertyName("orientation")]
        public double[] Orientation { get; set; }

        [JsonPropertyName("tiles")]
        public Tile[] Tiles { get; set; }
    }

    public class Tile
    {
        [JsonPropertyName("pos")]
        public long Pos { get; set; }

        [JsonPropertyName("face")]
        public long Face { get; set; }

        [JsonPropertyName("near")]
        public long[] Near { get; set; }

        [JsonPropertyName("vertices")]
        public double[][] Vertices { get; set; }

        [JsonPropertyName("offset")]
        public double[] Offset { get; set; }
    }

    public class Scale
    {
        [JsonPropertyName("size")]
        public double Size { get; set; }

        [JsonPropertyName("letters")]
        public double Letters { get; set; }
    }
}