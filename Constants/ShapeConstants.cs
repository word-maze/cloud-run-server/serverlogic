﻿using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using ServerLogic.Properties;

namespace ServerLogic.Constants
{
    public class ShapeConstants
    {
        public static Byte[][] ShapeData =
        {
            Resources.tetrahedron,
            Resources.cube,
            Resources.octahedron,
            Resources.icosahedron,
            Resources.dodecahedron
        };

        public static String[] ShapeDescription =
        {
            Encoding.UTF8.GetString(Resources.tetrahedron),
            Encoding.UTF8.GetString(Resources.cube),
            Encoding.UTF8.GetString(Resources.octahedron),
            Encoding.UTF8.GetString(Resources.icosahedron),
            Encoding.UTF8.GetString(Resources.dodecahedron)
        };

        public static Shape[] Shapes = ShapeDescription.Select(bytes => JsonSerializer.Deserialize<Shape>(bytes, new JsonSerializerOptions(){IncludeFields = true})).ToArray();
    }
}
