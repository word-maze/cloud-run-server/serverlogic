﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace ServerLogic
{
    public static class TaskExtensions
    {
        public static IEnumerable ToCoroutine<T>(this Task<T> task, Action<T> callback)
        {
            while (true)
            {
                if (task.IsCompleted)
                {
                    callback(task.Result);
                    yield break;
                }
                if (task.IsFaulted)
                {
                    throw task.Exception;
                }
                if (task.IsCanceled)
                {
                    yield break;
                }
                yield return null;
            }
        }
    }
}