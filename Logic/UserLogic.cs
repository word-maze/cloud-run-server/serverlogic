﻿using System;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace ServerLogic.Logic
{
    public class UserLogic : IUser
    {
        private readonly ILoadable _loader;
        private readonly IServer _server;

        public UserLogic(IServer server, ILoadable loader)
        {
            _loader = loader;
            _server = server;
        }

        public async Task<String> CreateNewUser()
        {
            return await _loader.Interact(async transaction =>
            {
                String uid = Guid.NewGuid().ToString();
                var userData = new UserData(uid);
                transaction.QueueSet(userData);
                return uid;
            });
        }

        public async Task Combine(String oldToken, String newToken)
        {
            //TODO implement user combining.
        }

        public async Task Announce(String uid)
        {
            await _loader.Interact(async transaction =>
            {
                var userData = await transaction.Get(UserData.CreateKey(uid));
                userData.LastInteraction = DateTime.UtcNow;
                transaction.QueueSet(userData);
                return uid;
            });
        }

        public async Task<UserData> GetUserData(String uid)
        {
            return await _loader.Gather(async transaction =>
            {
                return await transaction.Get(UserData.CreateKey(uid));
            });
        }
    }
}
