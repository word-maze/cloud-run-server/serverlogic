﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Classes;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace ServerLogic.Logic
{
    public class VersionLogic : IVersion
    {
        private ServerLogic _server;
        private ILoadable _loader;

        public VersionLogic(ServerLogic server, ILoadable loader)
        {
            _server = server;
            _loader = loader;
        }

        public async Task<VersionData> GetAssociatedVersion(Version clientVersion)
        {
            return await _loader.Gather(async transaction =>
            {
                var query = new List<QueryFilter>
                {
                    new QueryFilter("MajorClientVersion", FilterType.EQUALS, clientVersion.Major),
                    new QueryFilter("MinorClientVersion", FilterType.EQUALS, clientVersion.Minor)
                };

                var versions = await transaction.Query<VersionData>(query);

                if (versions.Count == 0)
                {
                    return new VersionData
                    {
                        ServerURL = ""
                    };
                }

                versions = versions.OrderBy(x => x.ClientVersion).ToList();

                return versions.First();
            });
        }

        public async Task Declare(Version serverVersion, String url)
        {
            await _loader.Interact(async transaction =>
            {
                var versions = await transaction.Query<ServerVersion>(new List<QueryFilter>());
                versions = versions.OrderBy(x => x.Version).ToList();

                ServerVersion version = new ServerVersion(serverVersion, url);

                if (versions.Count == 0 || versions.First().Version < serverVersion)
                {
                    transaction.QueueSet(version);
                }
            });
        }

        public async Task AssociateVersions(Version clientVersion, Version serverVersion)
        {
            await _loader.Interact(async transaction =>
            {
                var version =
                    await transaction.Get(ServerVersion.CreateKey(serverVersion.Major,
                        serverVersion.Minor));

                if (version != null)
                {
                    VersionData versionData = new VersionData(clientVersion, serverVersion, version.BaseURL);

                    transaction.QueueSet(versionData);
                }
            });
        }
    }
}
