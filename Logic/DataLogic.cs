﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using DataCore.Classes;
using DataCore.DataSystem;
using DataCore.Interfaces;
using DataCore.Responses;
using ServerLogic.Constants;

namespace ServerLogic.Logic
{
    public class DataLogic : IData
    {
        private readonly ILoadable _loader;
        private readonly IServer _server;

        public DataLogic(IServer server, ILoadable loader)
        {
            _loader = loader;
            _server = server;
        }

        public async Task<PuzzleMap> GetPuzzleMap(String date, Int32 shape, String uid)
        {
            var puzzleMap = await _loader.Interact(async transaction =>
            {
                var pmKey = PuzzleMap.CreateKey(date, shape);
                var udKey = UserData.CreateKey(uid);
                var results = await transaction.Get(pmKey, udKey);

                UserData userData = results.Get(udKey);
                DateTime puzzleDate = DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                if(!userData.Premium
                && puzzleDate < DateTime.UtcNow.AddDays(-7))
                {
                    return null;
                }

                return results.Get(pmKey);
            });

            return puzzleMap;
        }

        public async Task<String> GetShapeData(Int32 shape)
        {
            return Encoding.UTF8.GetString(ShapeConstants.ShapeData[shape]);
        }

        public async Task<FoundPuzzleWords> GetFoundWords(String date, Int32 shape, String uid)
        {
            var userPuzzleMap = await _loader.Interact(async transaction =>
            {
                UserPuzzleData userPuzzleData = await transaction.Get(UserPuzzleData.CreateKey(date, shape, uid));

                if (userPuzzleData == null)
                {
                    userPuzzleData = new UserPuzzleData(uid, date, shape);
                    transaction.QueueSet(userPuzzleData);
                }

                return userPuzzleData;
            });

            return new FoundPuzzleWords(userPuzzleMap.FoundWords);
        }
    }
}
