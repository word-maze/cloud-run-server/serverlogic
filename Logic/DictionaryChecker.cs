﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DataCore;
using DataCore.Responses;
using ServerLogic.Constants;
using ServerLogic.Properties;

namespace ServerLogic.Logic
{
    public class DictionaryChecker
    {
        public static String Letters = "abcdefghijklmnopqrstuvwxyz";
        public static Int32[] Points = { 1, 3, 3, 2, 1, 2, 3, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

        public static Dictionary<String, String[]> Words = JsonSerializer.Deserialize<Dictionary<String, String[]>>(Resources.DictionaryData, new JsonSerializerOptions());

        public static Boolean CheckWord(String word)
        {
            if(word.Length <= 1)
            {
                return false;
            }

            String key = word.Substring(0, 2);

            return Words.ContainsKey(key) && Words[key].Contains(word);
        }

        public static Int32 GetPoints(String word)
        {
            char[] letters = word.ToCharArray();

            Int32 points = letters.Aggregate(0, (total, character) => total + Points[character - 'a']);

            if (word.Length >= 10)
            {
                points *= 3;
            }

            if(word.Length >= 8)
            {
                points *= 2;
            }

            return points;
        }

        public static String[] WordList(String start)
        {
            List<String> options = new List<String>();

            if(!start.Contains('0'))
            {
                options.AddRange(Words[start]);
            }
            else if (start.Equals("00"))
            {
                for(Int32 i = 0; i < Letters.Length; i++)
                {
                    for(Int32 j = 0; j < Letters.Length; j++)
                    {
                        options.AddRange(Words[$"{Letters[i]}{Letters[j]}"]);
                    }
                }
            }
            else if (start[0] == '0')
            {
                for (Int32 i = 0; i < Letters.Length; i++)
                {
                    options.AddRange(Words[$"{Letters[i]}{start[1]}"]);
                }
            }
            else if (start[1] == '0')
            {
                for (Int32 i = 0; i < Letters.Length; i++)
                {
                    options.AddRange(Words[$"{start[0]}{Letters[i]}"]);
                }
            }

            return options.ToArray();
        }

        private static String[] FilterWords(String[] words, String wordCode)
        {
            Regex regex = new Regex("^" + wordCode.Replace("0", "[a-z]") + "[a-z]*");

            return words.Where(word => regex.IsMatch(word)).ToArray();
        }

        private static (Boolean, Char[]) Test(Int64[][] near, Char[] characters, Int64[] wordPositions, Int64 pos,
            String word, Int32 index, Int32 direction, Random rng)
        {
            if (index == word.Length || index == -1)
            {
                return (true, characters);
            }

            List<Int64> options = new List<Int64>();

            foreach (Int64 adjacent in near[pos])
            {
                if (characters[adjacent] == '0'
                    || characters[adjacent] == word[index])
                {
                    options.Add(adjacent);
                }
            }

            options = options.OrderBy(x => rng.Next()).ToList();

            foreach (Int64 option in options)
            {
                Char[] tempLetters = (Char[])characters.Clone();
                Int64[] tempPositions = (Int64[]) wordPositions.Clone();
                tempLetters[option] = word[index];
                tempPositions[index] = option;

                var result = Test(near, tempLetters, tempPositions, option, word, index + direction, direction, rng);

                if(result.Item1)
                {
                    for (int i = 0; i < wordPositions.Length; i++)
                    {
                        wordPositions[i] = tempPositions[i];
                    }

                    return result;
                }
            }

            return (false, characters);
        }

        private static void AltTest(String[] words, Int64[][] near, Char[] letters, Int64[] wordPositions, Int32 position, Int32 index, Dictionary<String, Int64[]> bank, Random rng)
        {
            if(index > 10)
            {
                return;
            }

            var options = near[position].OrderBy(x => rng.Next());

            foreach (Int64 option in options)
            {
                List<Int64> tempPositions = wordPositions.ToList();
                tempPositions.Add(option);

                String wordCode = String.Concat(tempPositions.Select(x => letters[x]));
                String[] tempWords = FilterWords(words, wordCode);

                if (tempPositions.Count > 3)
                {
                    foreach (String word in tempWords)
                    {
                        if (word.Length == tempPositions.Count)
                        {
                            bank[word] = tempPositions.ToArray();
                        }
                    }
                }

                if (tempWords.Length > 0)
                {
                    AltTest(tempWords, near, letters, tempPositions.ToArray(), (Int32)option, index + 1, bank, rng);
                }
            }
        }

        public static async Task<(Char[] map, List<WordData> createdWords)> Populate(Int32 shapeIndex, Random rng)
        {
            Shape shape = ShapeConstants.Shapes[shapeIndex];
            Int32 totalTiles = shape.Faces.Aggregate(0, (total, face) => total + face.Tiles.Length);

            Int64[][] near = new Int64[totalTiles][];

            foreach (Face face in shape.Faces)
            {
                foreach (Tile tile in face.Tiles)
                {
                    near[(Int32)tile.Pos] = tile.Near;
                }
            }

            Char[] letterPositions = new Char[totalTiles];
            Array.Fill(letterPositions, '0');

            Int32 wordLength = 10;
            List<WordData> wordList = new List<WordData>();

            do
            {
                String[] words = Words[$"{wordLength}"];
                String word = words.Pick(rng);

                List<Int32> positions = new List<Int32>();

                foreach (Char letter in word)
                {
                    for (Int32 pos = 0; pos < letterPositions.Length; pos++)
                    {
                        if (letter == letterPositions[pos])
                        {
                            positions.Add(pos);
                        }
                    }
                }

                for (Int32 i = 0; i < letterPositions.Length; i++)
                {
                    if (letterPositions[i] == '0')
                    {
                        positions.Add(i);
                    }
                }

                var tempPositions = positions.OrderBy(x => rng.Next());

                foreach (Int32 position in tempPositions)
                {
                    Int64[] wordPositions = new Int64[word.Length];
                    Array.Fill(wordPositions, -1);

                    Int32 startingPosition = 0;
                    if (letterPositions[position] != '0')
                    {
                        startingPosition = word.IndexOf(letterPositions[position]);
                    }

                    Char[] tempLetters = (Char[])letterPositions.Clone();
                    wordPositions[startingPosition] = position;
                    tempLetters[wordPositions[startingPosition]] = word[startingPosition];

                    Boolean fail = false;
                    if (startingPosition > 0)
                    {
                        var page = Test(near, tempLetters, wordPositions, position, word, startingPosition, -1, rng);

                        if (page.Item1)
                        {
                            tempLetters = page.Item2;
                        }
                        else
                        {
                            fail = true;
                        }
                    }

                    if (!fail)
                    {
                        var page = Test(near, tempLetters, wordPositions, position, word, startingPosition, 1, rng);

                        if (page.Item1)
                        {
                            tempLetters = page.Item2;
                        }
                        else
                        {
                            fail = true;
                        }
                    }

                    if(!fail)
                    {
                        letterPositions = tempLetters;
                        wordList.Add(new WordData(word, GetPoints(word), wordPositions));
                        break;
                    }
                }

                wordLength--;
            }
            while (wordLength > 7);

            /*for (Int32 attempt = 0; attempt < 100; attempt++)
            {
                Boolean blank = false;
                List<Int32> option = new List<Int32>();

                for (Int32 i = 0; i < letterPositions.Length; i++)
                {
                    if (letterPositions[i] != '0')
                    {
                        option.Add(i);
                    }
                    else
                    {
                        blank = true;
                    }
                }

                if (option.Count == 0 || !blank)
                {
                    break;
                }

                Int32 choice = option.Pick(rng);
                String[] possibleWords = WordList(letterPositions[choice].ToString() + '0');
                var bank = new Dictionary<String, Int64[]>();
                AltTest(possibleWords, near, letterPositions, new Int64[] { choice }, choice, 0, bank, rng);

                bank = bank.Where(x => wordList.All(y => x.Key != y.Word)).ToDictionary(x => x.Key, x => x.Value);
                //bank = bank.OrderByDescending(x => GetPoints(x.Key)).ToList();
                var foundWords = bank.OrderByDescending(x => GetPoints(x.Key)).ToList();

                if (foundWords.Count > 0)
                {
                    var picked = foundWords[0];
                    wordList.Add(new WordData(picked.Key, GetPoints(picked.Key), picked.Value));

                    for (Int32 i = 0; i < picked.Key.Length; i++)
                    {
                        letterPositions[picked.Value[i]] = picked.Key[i];
                    }
                }
            }*/

            for (Int32 i = 0; i < letterPositions.Length; i++)
            {
                while(letterPositions[i] == '0')
                {
                    Char chosen = Letters.ToCharArray().Pick(rng);
                    if (chosen != 'q')
                    {
                        letterPositions[i] = chosen;
                    }
                    else
                    {
                        var nearest = near[i].OrderBy(x => rng.Next());
                        foreach (Int64 pos in nearest)
                        {
                            if (letterPositions[pos] == '0'
                                || letterPositions[pos] == 'u')
                            {
                                letterPositions[i] = chosen;
                                letterPositions[pos] = 'u';
                            }
                        }
                    }
                }
            }

            wordList = wordList.OrderByDescending(word => GetPoints(word.Word)).ToList();

            return (letterPositions, wordList);
        }
    }
}
