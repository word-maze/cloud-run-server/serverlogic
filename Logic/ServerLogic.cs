﻿using DataCore.Interfaces;
using ServerLogic.Logic;

namespace ServerLogic
{
    public class ServerLogic : IServer
    {
        private readonly ILoadable _loader;

        public ServerLogic(ILoadable loader)
        {
            _loader = loader;

            OperationsAPI = new OperationsLogic(this, _loader);
            DataAPI = new DataLogic(this, _loader);
            UserAPI = new UserLogic(this, _loader);
            VersionAPI = new VersionLogic(this, _loader);
        }

        public IOperations OperationsAPI { get; }
        public IData DataAPI { get; }
        public IUser UserAPI { get; }
        public IVersion VersionAPI { get; }
    }
}
