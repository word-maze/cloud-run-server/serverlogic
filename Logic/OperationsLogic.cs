﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore;
using DataCore.Classes;
using DataCore.DataSystem;
using DataCore.Interfaces;
using DataCore.Responses;
using ServerLogic.Constants;

namespace ServerLogic.Logic
{
    public class OperationsLogic : IOperations
    {
        private readonly ILoadable _loader;
        private readonly IServer _server;

        public OperationsLogic(IServer server, ILoadable loader)
        {
            _loader = loader;
            _server = server;
        }

        public async Task GenerateDailyPuzzles()
        {
            String date = DateTime.UtcNow.ToString("yyyy-MM-dd");
            Random rng = new Random();

            var results = new (Char[] map, List<WordData> createdWords)[ShapeConstants.Shapes.Length];

            for (Int32 shapeIndex = 0; shapeIndex < ShapeConstants.Shapes.Length; shapeIndex++)
            {
                var populateResults = await DictionaryChecker.Populate(shapeIndex, rng);
                results[shapeIndex] = populateResults;
            }

            await _loader.Interact(async transaction =>
            {
                for (Int32 shapeIndex = 0; shapeIndex < ShapeConstants.Shapes.Length; shapeIndex++)
                {
                    (Char[] map, List<WordData> createdWords) = results[shapeIndex];

                    PuzzleMap puzzleMap = new PuzzleMap(date, shapeIndex)
                    {
                        Map = String.Concat(map),
                        CreatedWords = createdWords.ToArray(),
                        FoundWords =
                            createdWords.ToDictionary(word => Utils.PositionReference(word.Positions))
                    };

                    transaction.QueueSet(puzzleMap);
                }
            });
        }

        public async Task<FoundWord> CheckWord(String date, Int32 shape, String uid, WordData word)
        {
            return await _loader.Interact(async transaction =>
            {
                var pmKey = PuzzleMap.CreateKey(date, shape);
                var updKey = UserPuzzleData.CreateKey(date, shape, uid);
                var results = transaction.Get(pmKey, updKey);
                UserPuzzleData userPuzzleData = await results.Get(updKey);
                PuzzleMap puzzleMap = await results.Get(pmKey);

                if(userPuzzleData == null)
                {
                    userPuzzleData = new UserPuzzleData(uid, date, shape);
                    transaction.QueueSet(userPuzzleData);
                }

                FoundWord checkedResult = CheckPositions(puzzleMap, userPuzzleData, word);
                userPuzzleData.AddWord(new WordData(checkedResult.Word, checkedResult.Points, word.Positions));

                transaction.QueueSet(userPuzzleData, puzzleMap);

                return checkedResult;
            });
        }

        public async Task<FoundPuzzleWords> BatchCheck(String date, Int32 shape, String uid, WordList words)
        {
            return await _loader.Interact(async transaction =>
            {
                var pmKey = PuzzleMap.CreateKey(date, shape);
                var updKey = UserPuzzleData.CreateKey(date, shape, uid);
                var results = transaction.Get(pmKey, updKey);
                UserPuzzleData userPuzzleData = await results.Get(updKey);
                PuzzleMap puzzleMap = await results.Get(pmKey);

                if (userPuzzleData == null)
                {
                    userPuzzleData = new UserPuzzleData(uid, date, shape);
                    transaction.QueueSet(userPuzzleData);
                }

                List<WordData> foundWords = new List<WordData>();

                foreach (WordData word in words.Words)
                {
                    FoundWord checkedResult = CheckPositions(puzzleMap, userPuzzleData, word);
                    
                    userPuzzleData.AddWord(new WordData(checkedResult.Word, checkedResult.Points, word.Positions));

                    if (checkedResult.Points > 0)
                    {
                        foundWords.Add(new WordData(checkedResult.Word, checkedResult.Points, word.Positions));
                    }
                }

                transaction.QueueSet(userPuzzleData, puzzleMap);

                return new FoundPuzzleWords(foundWords);
            });
        }

        public async Task<WordList> RequestHint(String date, Int32 shape, String uid, Boolean buy)
        {
            return await _loader.Interact(async transaction =>
            {
                var pmKey = PuzzleMap.CreateKey(date, shape);
                var updKey = UserPuzzleData.CreateKey(date, shape, uid);
                var results = transaction.Get(pmKey, updKey);
                UserPuzzleData userPuzzleData = await results.Get(updKey);
                PuzzleMap puzzleMap = await results.Get(pmKey);

                if (userPuzzleData == null)
                {
                    userPuzzleData = new UserPuzzleData(uid, date, shape);
                    transaction.QueueSet(userPuzzleData);
                }

                if (!buy)
                {
                    return new WordList(userPuzzleData.Hints);
                }

                var foundWords = userPuzzleData.FoundWords;
                List<WordData> remainingHints = puzzleMap.CreatedWords
                    .Where(x => foundWords.Any(y => x.Word == y.Word)).ToList();

                return new WordList(remainingHints);
            });
        }

        public FoundWord CheckPositions(PuzzleMap map, UserPuzzleData userPuzzleData, WordData wordToCheck)
        {
            String reference = Utils.PositionReference(wordToCheck.Positions);
            WordData word;
            map.FoundWords.TryGetValue(reference, out word);

            if (word == null)
            {
                FoundWord checkResult = FullCheck(map, userPuzzleData, wordToCheck);
                if (checkResult.Found)
                {
                    word = wordToCheck;
                    word.Points = checkResult.Points;

                    map.AddWord(word);
                    userPuzzleData.AddWord(word);
                }
            }

            if(word != null)
            {
                return new FoundWord(true, word.Word, word.Points);
            }

            return new FoundWord(false, wordToCheck.Word, 0);
        }

        public FoundWord FullCheck(PuzzleMap map, UserPuzzleData userPuzzleData, WordData wordData)
        {
            Shape shape = ShapeConstants.Shapes[map.Shape];
            String word = "";
            Int32 previous = -1;
            Boolean adjacent = true;

            foreach(Int32 pos in wordData.Positions)
            {
                word += map.Map[pos];

                if (previous >= 0)
                {
                    Int32 tile = -1;
                    Int32 face = -1;
                    for (int faceIndex = 0; faceIndex < shape.Faces.Length; faceIndex++)
                    {
                        for (int positionIndex = 0;
                            positionIndex < shape.Faces[faceIndex].Tiles.Length;
                            positionIndex++)
                        {
                            if (shape.Faces[faceIndex].Tiles[positionIndex].Pos == previous)
                            {
                                tile = positionIndex;
                                face = faceIndex;
                            }
                        }
                    }

                    adjacent = adjacent && shape.Faces[face].Tiles[tile].Near.Contains(pos);
                }

                previous = pos;
            }

            var foundWord = adjacent
                            && userPuzzleData.FoundWords.Any(x => x.Positions.Equals(wordData.Positions))
                && DictionaryChecker.CheckWord(word);
            Int32 points = foundWord ? DictionaryChecker.GetPoints(word) : 0;

            return new FoundWord(foundWord, word, points);
        }
    }
}
